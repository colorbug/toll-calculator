import {timeStringToMinutes, isWeekend, parseHolidayData, isHoliday, sortDates, groupDatesByDaySorted} from '../src/dateHelpers'
import holidayDataRaw from '../src/data/holidays.json'

test('timeStringToMinutes should return 1110 with timestring 18:30', () => {
    const minutes = timeStringToMinutes('18:30')
    expect(minutes).toBe(1110)
})

test('timeStringToMinutes should throw error if wrong string format', () => {
    expect(() => timeStringToMinutes('1830')).toThrow(Error)
})

test('timeStringToMinutes should throw error if wrong hour or minute', () => {
    expect(() => timeStringToMinutes('24:67')).toThrow(Error)
})

test('2019-12-14 should be a weekend', () => {
    expect(isWeekend(new Date('2019-12-14'))).toBe(true)
})

test('2019-12-24 should be a holiday', () => {
    const holidayData = parseHolidayData(holidayDataRaw.data)
    expect(isHoliday(new Date('2019-12-24'), holidayData)).toBe(true)
})

test('should return dates sorted from low to high', () => {

    const datesToSort = [new Date('2019-12-10'), new Date('2019-12-05')]
    const sortedDates = sortDates(datesToSort)
    expect(sortedDates).toStrictEqual([new Date('2019-12-05'), new Date('2019-12-10')])
})


test('should return dates grouped by day (sorted by time)', () => {
    const dates = [
        new Date('2019-12-11 08:00:00'),
        new Date('2019-12-20 10:30:00'),
        new Date('2019-12-11 07:30:00'),
    ]
    const groups = groupDatesByDaySorted(dates)
    expect(groups).toStrictEqual({
            '2019-12-11': [new Date('2019-12-11 07:30:00'), new Date('2019-12-11 08:00:00')],
            '2019-12-20': [new Date('2019-12-20 10:30:00')]
        }
    )
})
