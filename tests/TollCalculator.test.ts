import TollCalculator from '../src/TollCalculator'
import {Car, Motorbike} from '../src/VehicleTypes'
import holidayData from '../src/data/holidays.json'
import tollRatesByTimeSpan from '../src/data/tollRatesByTimeSpan.json'

import {groupDatesByDaySorted, parseHolidayData, parseTollRates} from '../src/dateHelpers'

test('should return rate 8 for one occasion(/date)', () => {
    const holidayDates = parseHolidayData(holidayData.data)
    const tollRates = parseTollRates(tollRatesByTimeSpan.data)

    const tollCalculator = new TollCalculator(tollRates, holidayDates)

    const fee =  tollCalculator.getTollFeePerOccasion(new Date('2019-12-12 06:10:00'))
    expect(fee).toBe(8)
})

test('should return rates as array for multiple days', () => {
    const holidayDates = parseHolidayData(holidayData.data)
    const tollRates = parseTollRates(tollRatesByTimeSpan.data)

    const tollCalculator = new TollCalculator(tollRates, holidayDates)

    const dates = [
        new Date('2019-12-12 06:10:00'),
        new Date('2019-12-05 08:10:00'),
    ]
    const groupedDates = groupDatesByDaySorted(dates)
    const fee =  tollCalculator.getFeeRatePerDate(groupedDates)
    expect(fee).toStrictEqual([13, 8])
})


test('should be toll free vehicle', () => {
    const holidayDates = parseHolidayData(holidayData.data)
    const tollRates = parseTollRates(tollRatesByTimeSpan.data)

    const tollCalculator = new TollCalculator(tollRates, holidayDates)

    const vehicle = new Motorbike()

    const isTollFree =  tollCalculator.isTollFreeVehicle(vehicle)
    expect(isTollFree).toBe(true)
})



test('should be toll free day (weekend/holiday)', () => {
    const holidayDates = parseHolidayData(holidayData.data)
    const tollRates = parseTollRates(tollRatesByTimeSpan.data)

    const tollCalculator = new TollCalculator(tollRates, holidayDates)
    const isTollFree = tollCalculator.isTollFreeDate(new Date('2019-12-14 10:20:00'))

    expect(isTollFree).toBe(true)
})


test('should return rates for multiple', () => {
    const holidayDates = parseHolidayData(holidayData.data)
    const tollRates = parseTollRates(tollRatesByTimeSpan.data)

    const tollCalculator = new TollCalculator(tollRates, holidayDates)

    const vehicle = new Car()

    const dates = [
        new Date('2019-12-12 06:10:00'), // 8
        new Date('2019-12-12 10:30:00'), // 8
        new Date('2019-12-12 15:30:00'), // 18
    ]
    const fee =  tollCalculator.getTotalTollFee(vehicle, dates)
    expect(fee).toBe(34)
})


test('should return the highest rate within one hour', () => {
    const holidayDates = parseHolidayData(holidayData.data)
    const tollRates = parseTollRates(tollRatesByTimeSpan.data)

    const tollCalculator = new TollCalculator(tollRates, holidayDates)

    const vehicle = new Car()

    const dates = [
        new Date('2019-12-12 07:30:00'), // 18
        new Date('2019-12-12 07:34:00'), // excluded 18
        new Date('2019-12-12 08:00:00'), // excluded = 13
    ]
    const fee =  tollCalculator.getTotalTollFee(vehicle, dates)
    expect(fee).toBe(18)
})


test('should return rate for multiple days', () => {
    const holidayDates = parseHolidayData(holidayData.data)
    const tollRates = parseTollRates(tollRatesByTimeSpan.data)

    const tollCalculator = new TollCalculator(tollRates, holidayDates)

    const vehicle = new Car()

    const dates = [
        new Date('2019-12-05 07:30:00'), // Day 1: rate: 18
        new Date('2019-12-05 08:00:00'), // Day 1: rate: 13 (excluded)
        new Date('2019-12-05 10:30:00'), // Day 1: rate: 8
        new Date('2019-12-05 15:00:00'), // Day 1: rate: 13 (excluded)
        new Date('2019-12-05 15:30:00'), // Day 1: rate: 18
        new Date('2019-12-12 07:30:00'), // Day 2: rate: 18
        new Date('2019-12-12 08:00:00'), // Day 2: rate: 13 (excluded)
        new Date('2019-12-12 10:30:00'), // Day 2: rate: 8
        new Date('2019-12-12 15:00:00'), // Day 2: rate: 13 (excluded)
        new Date('2019-12-12 15:30:00'), // Day 2: rate: 18
    ]
    const fee =  tollCalculator.getTotalTollFee(vehicle, dates)
    expect(fee).toBe(88)
})

test('should return max toll (60 sek)', () => {
    const holidayDates = parseHolidayData(holidayData.data)
    const tollRates = parseTollRates(tollRatesByTimeSpan.data)

    const tollCalculator = new TollCalculator(tollRates, holidayDates)

    const vehicle = new Car()

    const dates = [
        new Date('2019-12-12 07:30:00'),
        new Date('2019-12-12 08:30:00'),
        new Date('2019-12-12 09:30:00'),
        new Date('2019-12-12 10:30:00'),
        new Date('2019-12-12 11:30:00'),
        new Date('2019-12-12 12:30:00'),
        new Date('2019-12-12 13:30:00'),
        new Date('2019-12-12 14:30:00'),
        new Date('2019-12-12 15:30:00'),
        new Date('2019-12-12 16:30:00'),
        new Date('2019-12-12 17:30:00'),
    ]
    const fee =  tollCalculator.getTotalTollFee(vehicle, dates)
    expect(fee).toBe(60)
})
