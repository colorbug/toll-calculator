export default interface Vehicle {
    getType(): string
    isTollFree: boolean
}
