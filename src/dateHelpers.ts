import {groupBy, Group} from './utils'
import TollRateByMinuteSpan from './TollRateByTime'

export function timeStringToMinutes(timeString: string): number {
    if (timeString.length !== 5 || timeString.indexOf(':') === -1) {
        throw new Error('Expected valid time string')
    }
    const timeArr = timeString.split(':')
    const hours = parseInt(timeArr[0])
    const minutes = parseInt(timeArr[1])

    if (hours > 23 || hours < 0) throw new Error('Expected valid hour')
    if (minutes > 59 || minutes < 0) throw new Error('Expected valid minutes')

    return hours * 60 + minutes
}

type TollRate = { fromTime: string, toTime: string, rate: number }

export function parseTollRates(tollRates: TollRate[]): TollRateByMinuteSpan[] {

    return tollRates.map(({fromTime, toTime, rate}) => {
        const fromTimeInMinutes = timeStringToMinutes(fromTime)
        const toTimeInMinutes = timeStringToMinutes(toTime)
        return new TollRateByMinuteSpan(fromTimeInMinutes, toTimeInMinutes, rate)
    })
}

export function parseHolidayData(holidayData: string[]): Date[] {
    return holidayData.map((holiday) => new Date(holiday))
}


export function isWeekend(date: Date): boolean {
    enum daysOfWeek {
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY
    }

    return date.getDay() == daysOfWeek.SATURDAY || date.getDay() == daysOfWeek.SUNDAY
}

export function isHoliday(date: Date, holidayDates: Date[]): boolean {
    return !!holidayDates.find((h) => dateToString(h) === dateToString(date))
}

export function sortDates(dates: Date[]): Date[] {
    return dates.sort((a, b) => a.getTime() - b.getTime())
}

export function withinTheHour(firstDate: Date, secondDate: Date): boolean {
    if (!firstDate || !secondDate) return false

    const diffInMillies = secondDate.getTime() - firstDate.getTime()
    const minutes = diffInMillies / 1000 / 60
    return minutes <= 60
}

export function dateToString(date: Date) {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
}


export function groupDatesByDaySorted(dates: Date[]): Group<Date> {
    const sortedDates = sortDates(dates)

    return groupBy(sortedDates, dateToString)
}
