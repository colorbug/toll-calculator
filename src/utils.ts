export function sum(arr: Array<number>): number {
    return arr.reduce((a, b) => a + b)
}

export type Group<Value> = { [_: string]: Array<Value> }

export function groupBy<Value>(values: Array<Value>, keyOf: (Value) => string): Group<Value> {
    return values.reduce((group: Group<Value>, value: Value) => {
        const key = keyOf(value)

        if (key in group)
            group[key].push(value)
        else
            group[key] = [value]

        return group
    }, {})
}

export function mapObj(collection: {}, fn: (val: any, key?: any) => any): Array<any> {
    const result = []
    for (const key in collection) {
        if (collection.hasOwnProperty(key)) {
            result.push(fn(collection[key], key))
        }
    }
    return result
}
