export default class TollRateByTime {
    private fromTimeInMinutes: number
    private toTimeInMinutes: number
    private rate: number

    constructor(fromTimeInMinutes: number, toTimeInMinutes: number, rate: number) {
        this.fromTimeInMinutes = fromTimeInMinutes
        this.toTimeInMinutes = toTimeInMinutes
        this.rate = rate
    }

    public getFromTime() {
        return this.fromTimeInMinutes
    }

    public getToTime() {
        return this.toTimeInMinutes
    }

    public getRate() {
        return this.rate
    }

}

