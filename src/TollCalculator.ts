import Vehicle from './Vehicle'
import TollRateByMinuteSpan from './TollRateByTime'
import {groupDatesByDaySorted, withinTheHour, isWeekend, isHoliday} from './dateHelpers'
import {Group, sum, mapObj} from './utils'

export default class TollCalculator {
    private readonly holidayDates: Date[]
    private readonly tollRatesByMinutes: TollRateByMinuteSpan[]


    constructor(tollRatesByMinutes: TollRateByMinuteSpan[], holidayDates: Date[]) {
        this.tollRatesByMinutes = tollRatesByMinutes
        this.holidayDates = holidayDates
    }

    /**
     * Calculate the total toll fee per vehicle
     *
     * @param vehicle - the vehicle
     * @param dates   - date and time of all passes
     * @return - the total toll fee for all dates
     */

    public getTotalTollFee(vehicle: Vehicle, dates: Date[]): number {

        if (this.isTollFreeVehicle(vehicle)) return 0
        if (dates.length === 0) return 0

        const datesByDay = groupDatesByDaySorted(dates)
        const feePerDay = this.getFeeRatePerDate(datesByDay)
        return sum(feePerDay)

    }
    public isTollFreeVehicle(vehicle: Vehicle) {
        return vehicle.isTollFree
    }

    public getFeeRatePerDate(datesByDay: Group<Date>) {
        return mapObj(datesByDay, (dates) => {
            const groupedFees = this.groupFeesByHourSpan(dates)
            const fees = groupedFees.map((rateGroup) => Math.max.apply(null, rateGroup))
            const totalFee = sum(fees)
            const maxFee = 60
            return Math.min(totalFee, maxFee)
        })
    }

    private groupFeesByHourSpan(sortedDates: Date[]) {
        const rates = []
        let intervalStartDate
        sortedDates.forEach((date) => {
            if (withinTheHour(intervalStartDate, date)) {
                const rate = this.getTollFeePerOccasion(date)
                rates[rates.length - 1].push(rate)
            } else {
                rates.push([this.getTollFeePerOccasion(date)])
                intervalStartDate = date
            }
        })
        return rates
    }

    public getTollFeePerOccasion(date: Date): number {
        if (this.isTollFreeDate(date)) return 0

        const hour = date.getHours()
        const minute = date.getMinutes()
        const timeOfDayInMinutes = hour * 60 + minute

        const currentRateByMinuteSpan = this.tollRatesByMinutes.find((span) => {
            return timeOfDayInMinutes >= span.getFromTime() && timeOfDayInMinutes <= span.getToTime()
        })
        return currentRateByMinuteSpan ? currentRateByMinuteSpan.getRate() : 0
    }

    public isTollFreeDate(date: Date): boolean {
        return isWeekend(date) || isHoliday(date, this.holidayDates)
    }

}
