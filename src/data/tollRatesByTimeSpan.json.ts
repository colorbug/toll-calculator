// This data would probably come from an updated database / api


export default {
    'data': [
        {
            'fromTime': '06:00',
            'toTime': '06:29',
            'rate': 8

        },
        {
            'fromTime': '06:30',
            'toTime': '06:59',
            'rate': 13

        },
        {
            'fromTime': '07:00',
            'toTime': '07:59',
            'rate': 18

        },
        {
            'fromTime': '08:00',
            'toTime': '08:29',
            'rate': 13

        },
        {
            'fromTime': '08:30',
            'toTime': '08:59',
            'rate': 8

        },
        {
            'fromTime': '09:30',
            'toTime': '09:59',
            'rate': 8

        },

        {
            'fromTime': '10:30',
            'toTime': '10:59',
            'rate': 8

        },
        {
            'fromTime': '11:30',
            'toTime': '11:59',
            'rate': 8

        },
        {
            'fromTime': '12:30',
            'toTime': '12:59',
            'rate': 8

        },
        {
            'fromTime': '13:30',
            'toTime': '13:59',
            'rate': 8

        },
        {
            'fromTime': '14:30',
            'toTime': '14:59',
            'rate': 8

        },
        {
            'fromTime': '15:00',
            'toTime': '15:29',
            'rate': 13

        },
        {
            'fromTime': '15:30',
            'toTime': '16:59',
            'rate': 18

        },
        {
            'fromTime': '17:00',
            'toTime': '17:59',
            'rate': 13

        },
        {
            'fromTime': '18:00',
            'toTime': '18:29',
            'rate': 8

        }
    ]
}
