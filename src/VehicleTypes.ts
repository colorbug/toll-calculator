import Vehicle from './Vehicle'

export class Car implements Vehicle {
    getType() {
        return 'Car'
    }

    isTollFree = false
}

export class Motorbike implements Vehicle {
    getType() {
        return 'Motorbike'
    }

    isTollFree = true
}

export class Tractor implements Vehicle {
    getType() {
        return 'Tractor'
    }

    isTollFree = true
}

export class Emergency implements Vehicle {
    getType() {
        return 'Emergency'
    }

    isTollFree = true
}

export class Diplomat implements Vehicle {
    getType() {
        return 'Diplomat'
    }

    isTollFree = true
}

export class Foreign implements Vehicle {
    getType() {
        return 'Foreign'
    }

    isTollFree = true
}

export class Military implements Vehicle {
    getType() {
        return 'Military'
    }

    isTollFree = true
}
